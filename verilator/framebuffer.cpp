// top=main::main

#include<iostream>
#include <memory>
#include <verilated.h>
#include "Vmain.h"
#include <mutex>
#include <thread>

#include <SFML/Graphics.hpp>


#define TICK \
    top->clk_i = true; \
    top->eval(); \
    top->clk_i = false; \
    top->eval();


constexpr int width = 1366;
constexpr int height = 768;

using pixels_t = std::array<std::array<sf::Uint8, width * height * 4>, 2>;


int main(int argc, char** argv) {

    std::cout << "test" << std::endl;

    // Create logs/ directory in case we have traces to put under it
    Verilated::mkdir("logs");

    // "VerilatedContext* contextp = new VerilatedContext" then deleting at end.
    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};

    // Verilator must compute traced signals
    contextp->traceEverOn(true);

    // Pass arguments so Verilated code can see them, e.g. $value$plusargs
    // This needs to be called before you create any model
    contextp->commandArgs(argc, argv);

    // Construct the Verilated model, from Vmain.h generated from Verilating "top.v".
    // Using unique_ptr is similar to "Vmain* top = new Vmain" then deleting at end.
    // "TOP" will be the hierarchical name of the module.
    const std::unique_ptr<Vmain> top{new Vmain{contextp.get(), "main"}};

    // Set Vmain's input signals

    top->rst_i = true;
    TICK
    top->rst_i = false;

    // Always accept data
    top->ack_i = true;
    top->err_i = false;
    top->data_out_i = 0;

    auto pixels = std::unique_ptr<pixels_t>(new pixels_t());
    std::mutex pixel_mutex;
    int index = 0;
    for(int i = 0; i < 2; i++) {
        for(int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                (*pixels)[i][(x + y*width) * 4] = 128;
                (*pixels)[i][(x + y*width) * 4+1] = 0;
                (*pixels)[i][(x + y*width) * 4+2] = 0;
                (*pixels)[i][(x + y*width) * 4+3] = 255;
            }
        }
    }


    std::thread t1([&index, &pixels, &pixel_mutex](){
        // create the window
        sf::RenderWindow window(sf::VideoMode(width, height), "verilator");

        sf::Texture texture;
        texture.create(width, height);

        sf::Sprite sprite;

        while (window.isOpen()) {
            {
                std::lock_guard<std::mutex> guard(pixel_mutex);
                // Flip display
                texture.update((*pixels)[index].data());
            }

            sprite.setTexture(texture);

            // check all the window's events that were triggered since the last iteration of the loop
            sf::Event event;
            while (window.pollEvent(event))
            {
                // "close requested" event: we close the window
                if (event.type == sf::Event::Closed) {
                    window.close();
                }
            }

            // clear the window with black color
            window.clear(sf::Color::Black);

            // draw everything here...
            // window.draw(...);
            window.draw(sprite);

            // end the current frame
            window.display();
        }
    });

    bool is_overflow = false;
    int prev_addr = 0;

    // Simulate until $finish
    while (true) {
    // for (int i = 0; (i < 100) && window.isOpen(); i++) {
        // std::cout << top->addr_o << " " << (top->write_o & 0xffff) << std::endl;
        TICK;

        int addr = top->addr_o;
        int write_x = addr % width;
        int write_y = addr / width;


        {
            std::lock_guard<std::mutex> guard(pixel_mutex);
            if(top->drawer_out_o == true) {
                index = !index;
                std::cout << "flip" << std::endl;

                std::cout << write_x << ", " << write_y << std::endl;

                for(int x = 0; x < width; x++) {
                    for (int y = 0; y < height; y++) {
                        (*pixels)[!index][(x + y*width) * 4] = 0;
                        (*pixels)[!index][(x + y*width) * 4+1] = 0;
                        (*pixels)[!index][(x + y*width) * 4+2] = 128;
                        (*pixels)[!index][(x + y*width) * 4+3] = 255;
                    }
                }
            }

            if (addr < 0 || addr > width*height) {
                if (!is_overflow) {
                    std::cout << "Bad address " << addr << " prev: " << prev_addr << std::endl;
                }
                is_overflow = true;
                continue;
            }
            else {
                is_overflow = false;
            }
            prev_addr = addr;

            (*pixels)[!index][(write_x + (write_y * width)) * 4] = top->write_o & 0xff;
            (*pixels)[!index][(write_x + (write_y * width)) * 4+1] = (top->write_o >> 8) & 0xff;
        }

    }

    // Final model cleanup
    top->final();
    
    t1.join();

    return 0;
}
